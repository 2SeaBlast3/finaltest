thisApp.controller("editOne", function ($scope,$http, $routeParams,$location){
	$scope.modelOne = {};
	$scope.modelOne.name= "";
	$scope.modelOne.modelTwoID= "";
	$scope.modelTwos = [];
	
	var url = "/api/modelOnes/"+ $routeParams.id;
	var UrlTwos = "/api/modelTwos";
	
	
	
	var getTwos = function (){
		var promice = $http.get(UrlTwos);
		promice.then(
				function success(res){
					$scope.modelTwos =res.data;
				},
				function fail(res){
					alert("could not get all Twos");
				}
		)
	}
	
	getTwos();
	
	
	var getOne = function(){
		var promice = $http.get(url);
		promice.then(
				function success(res){
					$scope.modelOne =res.data;
				},
				function fail(res){
					alert("could not get One");
				}
		)
		
	}
	getOne();
	
	
	
	$scope.doEdit = function(){
		$http.put(url, $scope.modelOne).then(
			function success(){
				$location.path("/");
			},
			function error(){
				alert("Something went wrong.");
			}
		);
	}
	
	
	
	
});