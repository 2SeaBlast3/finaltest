package origin.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ModelThree {
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private ModelOne modelOne;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ModelOne getModelOne() {
		return modelOne;
	}

	public void setModelOne(ModelOne modelOne) {
		this.modelOne = modelOne;
		if(modelOne.getModelThrees().contains(this)) {
			modelOne.getModelThrees().add(this);
		}
	}
	
	
}
