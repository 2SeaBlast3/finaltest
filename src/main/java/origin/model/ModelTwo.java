package origin.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ModelTwo {
	
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String naziv;
	@Column
	private String adresa;
	@Column
	private String telefon;
	
	
	@OneToMany(mappedBy="modelTwo", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<ModelOne> modelOnes =new ArrayList<ModelOne>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public List<ModelOne> getModelOnes() {
		return modelOnes;
	}

	public void setModelOnes(List<ModelOne> modelOnes) {
		this.modelOnes = modelOnes;
	}
	
	public void addModelOne(ModelOne modelOne) {
		
		if(modelOne.getModelTwo() != null && modelOne.getModelTwo().equals(this)) {
			modelOne.setModelTwo(this);
		}
		modelOnes.add(modelOne);
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	
	
}
