package origin.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ModelOne {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String name;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private ModelTwo modelTwo;
	
	@OneToMany(mappedBy = "modelOne", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ModelThree> modelThrees = new ArrayList<>();

	public ModelTwo getModelTwo() {
		return modelTwo;
	}

	public void setModelTwo(ModelTwo modelTwo) {
		this.modelTwo = modelTwo;
		if(modelTwo.getModelOnes().contains(this)) {
			modelTwo.getModelOnes().add(this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ModelThree> getModelThrees() {
		return modelThrees;
	}

	public void setModelThrees(List<ModelThree> modelThrees) {
		this.modelThrees = modelThrees;
	}
	

public void addModelThree(ModelThree modelThree) {
		
		if(modelThree.getModelOne() != null && modelThree.getModelOne().equals(this)) {
			modelThree.setModelOne(this);
		}
		modelThrees.add(modelThree);
	}
	
	
	

}
