package origin.web.dto;

import java.util.ArrayList;
import java.util.List;


import origin.model.ModelOne;

public class TwoDTO {

	private Long id;
	private String naziv;
	private String adresa;
	private String telefon;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	
	
}
