package origin.web.dto;


public class OneDTO {
	private Long id;
	
	private String name;
	
	private Long modelTwoID;
	
	private String ModelTwoname;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getModelTwoID() {
		return modelTwoID;
	}

	public void setModelTwoID(Long modelTwoID) {
		this.modelTwoID = modelTwoID;
	}

	public String getModelTwoname() {
		return ModelTwoname;
	}

	public void setModelTwoname(String modelTwoname) {
		ModelTwoname = modelTwoname;
	}
	
	
}
