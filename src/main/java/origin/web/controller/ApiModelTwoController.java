package origin.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import origin.model.ModelOne;
import origin.model.ModelTwo;
import origin.service.ModelOneService;
import origin.service.ModelTwoService;
import origin.support.DTOtoTwo;
import origin.support.OneToDTO;
import origin.support.TwoToDTO;
import origin.web.dto.OneDTO;
import origin.web.dto.TwoDTO;

@RestController
@RequestMapping( value = "/api/modelTwos")
public class ApiModelTwoController {
	
	@Autowired
	ModelTwoService modelTwoService;
	@Autowired
	TwoToDTO twoToDTO;
	@Autowired
	DTOtoTwo dTOtoTwo;
	@Autowired
	ModelOneService modelOneService;
	@Autowired
	OneToDTO oneToDTO;
	
	// 	FOR GET ALL
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<TwoDTO>> getAll() {

		List<ModelTwo> all = modelTwoService.findAll();

		return new ResponseEntity<List<TwoDTO>>(twoToDTO.convert(all), HttpStatus.OK);
	}
	
//	@RequestMapping(method = RequestMethod.GET)
//	public ResponseEntity<List<ModelTwo>> getAll() {
//
//		List<ModelTwo> all = modelTwoService.findAll();
//
//		return new ResponseEntity<List<ModelTwo>>(all, HttpStatus.OK);
//	}
	
	
	// FOR GET ONE!!!
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<TwoDTO> getOne(@PathVariable Long id) {

		ModelTwo one = modelTwoService.findOne(id);

		if (one == null) {

			return new ResponseEntity<TwoDTO>(HttpStatus.NOT_FOUND);

		}

		return new ResponseEntity<TwoDTO>(twoToDTO.convert(one), HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
//	public ResponseEntity<ModelTwo> getOne(@PathVariable Long id) {
//
//		ModelTwo one = modelTwoService.findOne(id);
//
//		if (one == null) {
//
//			return new ResponseEntity<ModelTwo>(HttpStatus.NOT_FOUND);
//
//		}
//
//		return new ResponseEntity<ModelTwo>(one, HttpStatus.OK);
//	}
	
	
	
	
	// for EDIT
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<TwoDTO> edit(@PathVariable Long id, @RequestBody TwoDTO twoDTO) {


		if (!id.equals(twoDTO.getId())) {

			return new ResponseEntity<TwoDTO>(HttpStatus.BAD_REQUEST);

		}
		
		ModelTwo edited = modelTwoService.save(dTOtoTwo.convert(twoDTO));


		return new ResponseEntity<TwoDTO>(twoToDTO.convert(edited), HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes="application/json")
//	public ResponseEntity<ModelTwo> edit(@PathVariable Long id, @RequestBody ModelTwo modelTwo) {
//
//
//		if (!id.equals(modelTwo.getId())) {
//
//			return new ResponseEntity<ModelTwo>(HttpStatus.BAD_REQUEST);
//
//		}
//		
//		ModelTwo edited = modelTwoService.save(modelTwo);
//
//
//		return new ResponseEntity<ModelTwo>(edited, HttpStatus.OK);
//	}
	
	// for ADD new OBJ
	
	@RequestMapping( method = RequestMethod.POST, consumes="application/json")
	public ResponseEntity<TwoDTO> edit( @RequestBody TwoDTO twoDTO) {


		
		
		ModelTwo edited = modelTwoService.save(dTOtoTwo.convert(twoDTO));


		return new ResponseEntity<TwoDTO>(twoToDTO.convert(edited), HttpStatus.CREATED);
	}
	
	
//	@RequestMapping( method = RequestMethod.POST, consumes="application/json")
//	public ResponseEntity<ModelTwo> edit( @RequestBody ModelTwo modelTwo) {
//
//
//		
//		
//		ModelTwo edited = modelTwoService.save(modelTwo);
//
//
//		return new ResponseEntity<ModelTwo>(edited, HttpStatus.OK);
//	}
	
	
	// for Delete
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<TwoDTO> delete(@PathVariable Long id) {

		ModelTwo one = modelTwoService.findOne(id);

		if (one == null) {

			return new ResponseEntity<TwoDTO>(HttpStatus.NOT_FOUND);

		}
		
		modelTwoService.delete(id);

		return new ResponseEntity<TwoDTO>(twoToDTO.convert(one), HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<ModelTwo> delete(@PathVariable Long id) {
//
//		ModelTwo one = modelTwoService.findOne(id);
//
//		if (one == null) {
//
//			return new ResponseEntity<ModelTwo>(HttpStatus.NOT_FOUND);
//
//		}
//		
//		modelTwoService.delete(id);
//
//		return new ResponseEntity<ModelTwo>(one, HttpStatus.OK);
//	}
	
	
	@RequestMapping(value = "/{id}/ModelOne", method = RequestMethod.GET)
	public ResponseEntity<List<OneDTO>> special(@PathVariable Long id) {
		List<ModelOne> lista= modelOneService.findByModelTwoId(id);

		if (lista == null) {

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		}
		

		return new ResponseEntity<>(oneToDTO.convert(lista), HttpStatus.OK);
	}
	
	
	
	
	
	
	@ExceptionHandler(value=DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
	}
	

}
