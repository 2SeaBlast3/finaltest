package origin.web.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import origin.model.ModelOne;
import origin.model.ModelThree;
import origin.service.ModelOneService;
import origin.support.DTOtoOne;
import origin.support.OneToDTO;
import origin.support.ThreeToDTO;
import origin.web.dto.OneDTO;
import origin.web.dto.ThreeDTO;





@RestController
@RequestMapping(value = "/api/modelOnes")
public class ApiModelOneController {

	@Autowired
	ModelOneService modelOneService;
	
	@Autowired
	OneToDTO oneToDTO;
	@Autowired
	DTOtoOne dTOtoOne;
	
	@Autowired
	ThreeToDTO threeToDTO;
	

	// this one is for get!!!
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<OneDTO>> getAll(@RequestParam(required=false) String name ,@RequestParam(value="pageNum", defaultValue="0") int pageNum) {
		Page<ModelOne> OnePage = null;
		HttpHeaders headers = new HttpHeaders();
		
		if(name !=null) {
			OnePage = modelOneService.serch(name,pageNum);
		}
		else {
		
		OnePage = modelOneService.findAll(pageNum);
		}
		headers.add("totalPages", Integer.toString(OnePage.getTotalPages()) );
		
		return new ResponseEntity<>(oneToDTO.convert(OnePage.getContent()), headers,HttpStatus.OK);
	}
	
	
//	@RequestMapping(method = RequestMethod.GET)
//	public ResponseEntity<List<ModelOne>> getAll() {
//
//		List<ModelOne> all = modelOneService.findAll();
//
//		return new ResponseEntity<List<ModelOne>>(all, HttpStatus.OK);
//	}
	
	

	
	
	
	// This one is for get just one! 
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<OneDTO> getOne(@PathVariable Long id) {

		ModelOne one = modelOneService.findOne(id);

		if (one == null) {

			return new ResponseEntity<OneDTO>(HttpStatus.NOT_FOUND);

		}

		return new ResponseEntity<OneDTO>(oneToDTO.convert(one), HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
//	public ResponseEntity<ModelOne> getOne(@PathVariable Long id) {
//
//		ModelOne one = modelOneService.findOne(id);
//
//		if (one == null) {
//
//			return new ResponseEntity<ModelOne>(HttpStatus.NOT_FOUND);
//
//		}
//
//		return new ResponseEntity<ModelOne>(one, HttpStatus.OK);
//	}
	
	
	
	
	//FOR EDIT!!!!!!!
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<OneDTO> edit(@PathVariable Long id, @RequestBody OneDTO oneDTO) {


		if (!id.equals(oneDTO.getId())) {

			return new ResponseEntity<OneDTO>(HttpStatus.BAD_REQUEST);

		}
		
		ModelOne edited = modelOneService.save(dTOtoOne.convert(oneDTO));


		return new ResponseEntity<OneDTO>(oneToDTO.convert(edited), HttpStatus.OK);
	}
	
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes="application/json")
//	public ResponseEntity<ModelOne> edit(@PathVariable Long id, @RequestBody ModelOne modelOne) {
//
//
//		if (!id.equals(modelOne.getId())) {
//
//			return new ResponseEntity<ModelOne>(HttpStatus.BAD_REQUEST);
//
//		}
//		
//		ModelOne edited = modelOneService.save(modelOne);
//
//
//		return new ResponseEntity<ModelOne>(edited, HttpStatus.OK);
//	}
	
	
	
	
	
	// for ADDing NEW Obj
	
	
	@RequestMapping( method = RequestMethod.POST, consumes="application/json")
	public ResponseEntity<OneDTO> add(@Validated @RequestBody OneDTO oneDTO) {


		
		
		ModelOne added = modelOneService.save(dTOtoOne.convert(oneDTO));


		return new ResponseEntity<OneDTO>(oneToDTO.convert(added), HttpStatus.CREATED);
	}
	
//	@RequestMapping( method = RequestMethod.POST, consumes="application/json")
//	public ResponseEntity<ModelOne> edit( @RequestBody ModelOne modelOne) {
//
//
//		
//		
//		ModelOne added = modelOneService.save(modelOne);
//
//
//		return new ResponseEntity<ModelOne>(added, HttpStatus.OK);
//	}
	
	
	
	// FOR DELETE!!!!!
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<OneDTO> delete(@PathVariable Long id) {

		ModelOne one = modelOneService.findOne(id);

		if (one == null) {

			return new ResponseEntity<OneDTO>(HttpStatus.NOT_FOUND);

		}
		
		modelOneService.delete(id);

		return new ResponseEntity<OneDTO>(oneToDTO.convert(one), HttpStatus.OK);
	}
	
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<ModelOne> delete(@PathVariable Long id) {
//
//		ModelOne one = modelOneService.findOne(id);
//
//		if (one == null) {
//
//			return new ResponseEntity<ModelOne>(HttpStatus.NOT_FOUND);
//
//		}
//		
//		modelOneService.delete(id);
//
//		return new ResponseEntity<ModelOne>(one, HttpStatus.OK);
//	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{id}")
	public ResponseEntity<ThreeDTO> Something(@PathVariable Long id){
		 ModelThree ret= null;
		 ret= modelOneService.something(id);
		if(ret != null) {
			return new ResponseEntity<>(threeToDTO.convert(ret),
					HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<>(
					HttpStatus.BAD_REQUEST);
		}
			
	}
	
	
	
	
	@ExceptionHandler(value=DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
	}

	

}