package origin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import origin.model.ModelTwo;

@Repository
public interface MTwoRepository extends JpaRepository<ModelTwo, Long>{

}
