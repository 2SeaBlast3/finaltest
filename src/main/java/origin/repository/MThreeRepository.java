package origin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import origin.model.ModelThree;

@Repository
public interface MThreeRepository extends JpaRepository<ModelThree, Long> {

}
