package origin.repository;

import org.springframework.data.repository.query.Param;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import origin.model.ModelOne;

@Repository
public interface MOneRepository extends JpaRepository<ModelOne, Long>{
	@Query("SELECT m FROM ModelOne m WHERE " 
			+"(:modelOneName IS NULL OR m.name like :modelOneName)")
	Page<ModelOne> serch(@Param("modelOneName") String modelOneName,Pageable page);

	List<ModelOne> findByModelTwoId(Long id);

}
