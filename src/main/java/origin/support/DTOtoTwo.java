package origin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import origin.model.ModelTwo;
import origin.service.ModelTwoService;
import origin.web.dto.TwoDTO;

@Component
public class DTOtoTwo  implements Converter<TwoDTO, ModelTwo>{
	@Autowired
	ModelTwoService  modelTwoService;
	@Override
	public ModelTwo convert(TwoDTO source) {
		
		ModelTwo modelTwo = null;
		
		if(source.getId() == null){
			modelTwo = new ModelTwo();
		}else {
			modelTwo = modelTwoService.findOne(source.getId());
		}
		
		
		//TODO   izmeniti po modelu:
		modelTwo.setId(source.getId());

		modelTwo.setNaziv(source.getNaziv());
		modelTwo.setAdresa(source.getAdresa());
		modelTwo.setTelefon(source.getTelefon());
		
		
		
		return modelTwo;
	}
	
	public List<ModelTwo> convert(List<TwoDTO> source){
		List<ModelTwo> ret = new ArrayList<ModelTwo>();
		
		for(TwoDTO dto: source){
			ret.add(convert(dto));
		}
		
		return ret;
	}
	

}
