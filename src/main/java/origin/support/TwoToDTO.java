package origin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import origin.model.ModelTwo;
import origin.service.ModelTwoService;
import origin.web.dto.TwoDTO;

@Component
public class TwoToDTO implements Converter<ModelTwo, TwoDTO>{
	
	
	@Autowired
	ModelTwoService modelTwoService;
	@Override
	public TwoDTO convert(ModelTwo source) {
		TwoDTO  dto = new TwoDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setAdresa(source.getAdresa());
		dto.setTelefon(source.getTelefon());
		
		return dto;
	}
	
	
	public List<TwoDTO> convert(List<ModelTwo> source){
		List<TwoDTO> dto = new ArrayList<TwoDTO>();
		
		for(ModelTwo m: source) {
			dto.add(convert(m));
		}
		
		return dto;
	}
	
	

}
