package origin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import origin.model.ModelOne;
import origin.web.dto.OneDTO;

@Component
public class OneToDTO implements Converter<ModelOne, OneDTO>{

	@Override
	public OneDTO convert(ModelOne source) {
		
		OneDTO ondeDTO = new OneDTO();
		
		ondeDTO.setId(source.getId());
		ondeDTO.setName(source.getName());
		ondeDTO.setModelTwoID(source.getModelTwo().getId());
		ondeDTO.setModelTwoname(source.getModelTwo().getNaziv());
		// TODO Auto-generated method stub
		return ondeDTO;
	}
	
	public List<OneDTO>  convert(List<ModelOne> source){
		
		List<OneDTO>  ondeDTOs = new ArrayList<OneDTO>();
		
		for(ModelOne m : source) {
			ondeDTOs.add(convert(m));
			
		}
		
		
		return ondeDTOs;
		
	}
	

}
