package origin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import origin.model.ModelOne;
import origin.model.ModelTwo;
import origin.service.ModelOneService;
import origin.service.ModelTwoService;
import origin.web.dto.OneDTO;

@Component
public class DTOtoOne implements Converter<OneDTO, ModelOne> {
	@Autowired
	ModelOneService modelOneService;
	@Autowired
	ModelTwoService modelTwoService;

	@Override
	public ModelOne convert(OneDTO source) {
		ModelTwo modelTwo = modelTwoService.findOne(source.getModelTwoID());
		if (modelTwo != null) {
			ModelOne modelOne = null;
			if (source.getId() != null) {
				modelOne = modelOneService.findOne(source.getId());
			} else {
				modelOne = new ModelOne();
			}

			modelOne.setId(source.getId());
			modelOne.setName(source.getName());
			modelOne.setModelTwo(modelTwoService.findOne(source.getModelTwoID()));

			return modelOne;
		} else {
			throw new IllegalStateException("Trying to attach to non-existant entities");
		}
	}

	public List<ModelOne> convert(List<OneDTO> source) {

		List<ModelOne> ret = new ArrayList<ModelOne>();

		for (OneDTO o : source) {
			ret.add(convert(o));
		}

		return ret;

	}

}
