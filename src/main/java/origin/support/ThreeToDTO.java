package origin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import origin.model.ModelThree;
import origin.web.dto.ThreeDTO;

@Component
public class ThreeToDTO implements Converter<ModelThree, ThreeDTO> {

	@Override
	public ThreeDTO convert(ModelThree source) {
		ThreeDTO dto = new ThreeDTO();
		
		dto.setId(source.getId());
		dto.setModelOneid(source.getModelOne().getId());
		
		return dto;
	}
	
	public List<ThreeDTO> convert(List<ModelThree> source ){
		List<ThreeDTO> list = new ArrayList<ThreeDTO>();
		
		for (ModelThree m: source) {
			
			
			list.add(convert(m));
		}
		
		return list;
	}

}
