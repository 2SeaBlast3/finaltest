package origin.service;

import java.util.List;

import origin.model.ModelTwo;

public interface ModelTwoService {
	List<ModelTwo> findAll();
	
	ModelTwo findOne (Long id);
	
	ModelTwo save (ModelTwo modelTwo);
	
	
	ModelTwo delete (Long id);

}
