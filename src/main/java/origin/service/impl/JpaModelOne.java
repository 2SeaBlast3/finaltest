package origin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import origin.model.ModelOne;
import origin.model.ModelThree;
import origin.repository.MOneRepository;
import origin.repository.MThreeRepository;
import origin.service.ModelOneService;

@Service
public class JpaModelOne implements ModelOneService {
	
	@Autowired
	MOneRepository 	mOneRepository;
	@Autowired
	MThreeRepository mThreeRepository;
	

	@Override
	public Page<ModelOne> findAll(int pageNum) {
		// TODO Auto-generated method stub
		return mOneRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public ModelOne findOne(Long id) {
		// TODO Auto-generated method stub
		return mOneRepository.findOne(id);
	}

	@Override
	public ModelOne save(ModelOne modelOne) {
		// TODO Auto-generated method stub
		return mOneRepository.save(modelOne);
	}

	@Override
	public ModelOne delete(Long id) {
		ModelOne deleted = mOneRepository.findOne(id);
		
		mOneRepository.delete(id);
		return deleted;
	}

	@Override
	public Page<ModelOne> serch(String modelOneName, int pageNum) {
		if(modelOneName != null) {
			modelOneName = "%" + modelOneName +"%";
		}
		return mOneRepository.serch(modelOneName,new PageRequest(pageNum, 5));
	}

	@Override
	public ModelThree something(Long id) {
		ModelOne modelOne = findOne(id);
		if(modelOne != null) {
			ModelThree modelThree = null;
//			if(Logika) {
//				modelThree = new ModelThree();
//				modelThree.setModelOne(modelOne);
//				mThreeRepository.save(modelThree);
//				
//			//	l.setBrojMesta(l.getBrojMesta() - 1); neka radnja
//				mOneRepository.save(modelOne);
//			}
			
			return null;//modelThree
		}
		return null;
	}

	@Override
	public List<ModelOne> findByModelTwoId(Long id) {
		return mOneRepository.findByModelTwoId(id);
	}
	
	

}
