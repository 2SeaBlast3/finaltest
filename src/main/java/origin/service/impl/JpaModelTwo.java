package origin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import origin.model.ModelTwo;
import origin.repository.MTwoRepository;
import origin.service.ModelTwoService;

@Service
public class JpaModelTwo implements ModelTwoService{
	
	@Autowired
	MTwoRepository mTwoRepository;

	@Override
	public List<ModelTwo> findAll() {
		// TODO Auto-generated method stub
		return mTwoRepository.findAll();
	}

	@Override
	public ModelTwo findOne(Long id) {
		// TODO Auto-generated method stub
		return mTwoRepository.findOne(id);
	}

	@Override
	public ModelTwo save(ModelTwo modelTwo) {
		// TODO Auto-generated method stub
		return mTwoRepository.save(modelTwo);
	}

	@Override
	public ModelTwo delete(Long id) {
		ModelTwo deleted = mTwoRepository.findOne(id);
		mTwoRepository.delete(id);
		
		return deleted;
	}

}
