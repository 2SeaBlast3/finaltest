package origin.service;

import java.util.List;

import org.springframework.data.domain.Page;

import origin.model.ModelOne;
import origin.model.ModelThree;

public interface ModelOneService {
	
	Page<ModelOne> findAll(int pageNum);
	
	ModelOne findOne(Long id);
	
	ModelOne save(ModelOne modelOne);
	
	ModelOne delete(Long id);

	Page<ModelOne> serch(String modelOneName, int pageNum);

	ModelThree something(Long id);

	List<ModelOne> findByModelTwoId(Long id);
}
