package origin;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import origin.model.ModelOne;
import origin.model.ModelTwo;
import origin.service.ModelOneService;
import origin.service.ModelTwoService;
@Component
public class TestData {
	
	@Autowired
	ModelOneService modelOneService;
	@Autowired
	ModelTwoService modelTwoService;
	
	
	@PostConstruct
	public void init() {
		ModelTwo modelTwo = new ModelTwo();
		modelTwo.setNaziv("ILOVEYOU");
		modelTwo.setAdresa("Neka Adresa");
		modelTwo.setTelefon("021-666-999");
		modelTwoService.save(modelTwo);
		
		ModelTwo modelTwo1 = new ModelTwo();
		modelTwo1.setNaziv("Storm Worm trojan");
		modelTwo1.setAdresa("Druga Adresa ");
		modelTwo1.setTelefon("021-666-777");
		modelTwoService.save(modelTwo1);
		
		ModelTwo modelTwo2 = new ModelTwo();
		modelTwo2.setNaziv("myDoom");
		modelTwo2.setAdresa("Treca Adresa ");
		modelTwo2.setTelefon("021-555-777");
		modelTwoService.save(modelTwo2);
		

		
		
		ModelOne modelOne = new ModelOne();
		modelOne.setName("ILOVEYOU");
		modelOne.setModelTwo(modelTwo);
		modelOneService.save(modelOne);
		modelTwo.addModelOne(modelOne);
		modelTwoService.save(modelTwo);
		
		
		ModelOne modelOne1 = new ModelOne();
		modelOne1.setName(" Storm Worm trojan");
		modelOne1.setModelTwo(modelTwo2);
		modelOneService.save(modelOne1);
		modelTwo2.addModelOne(modelOne1);
		modelTwoService.save(modelTwo2);
		
		ModelOne modelOne2 = new ModelOne();
		modelOne2.setName("myDoom");
		modelOne2.setModelTwo(modelTwo2);
		modelOneService.save(modelOne2);
		modelTwo2.addModelOne(modelOne2);
		modelTwoService.save(modelTwo2);
			
		
		}
		
	
	
	
}
