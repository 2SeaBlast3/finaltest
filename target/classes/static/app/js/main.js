var thisApp = angular.module("thisApp", ["ngRoute"]);



thisApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	
	.when('/', {
		templateUrl : '/app/html/exe.html',
			controller : 'executive'
	})
//	.when('/modelOnes/add', {
//		templateUrl : '/app/html/addOnes.html'
//	})
	.when('/edit/:id', {
		templateUrl : '/app/html/editOne.html',
			controller : 'editOne'
	})
	.otherwise({
		redirectTo: '/'
	});
}]);
