thisApp.controller("executive", function ($scope,$http,$location){
	
	
	$scope.modelOnes = [];
	$scope.modelTwos = [];
	
	$scope.newModelOne = {};
	$scope.newModelOne.id="";
	$scope.newModelOne.name="";
	$scope.newModelOne.modelTwoID="";
	
	$scope.searchParams = {};
	$scope.searchParams.name = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var url = "/api/modelOnes";
	
	var UrlTwos = "/api/modelTwos";
	
	var config = {params:{}};
	
	var getAll = function(){
		
		
		if($scope.searchParams.name != ""){
			config.params.name = $scope.searchParams.name;
		}
		config.params.pageNum = $scope.pageNum;
		
		var promice = $http.get(url,config);
		promice.then(
				function success(res){
					$scope.modelOnes =res.data;
					$scope.totalPages = res.headers("totalPages");
					
					console.log($scope.modelOnes);
					config.params.name= "";
				},
				function fail(res){
					alert("could not get all");
				}
		)
		
	}
	
	getAll();
	
	var getTwos = function (){
		var promice = $http.get(UrlTwos);
		promice.then(
				function success(res){
					$scope.modelTwos =res.data;
				},
				function fail(res){
					alert("could not get all Twos");
				}
		)
	}
	
	getTwos();
	
	$scope.doAdd = function (){
		var promice = $http.post(url,$scope.newModelOne);
		promice.then(
				function success(res){
					getAll();
					

					$scope.newModelOne.id="";
					$scope.newModelOne.name="";
					$scope.newModelOne.modelTwoID="";
					
				},
				function fail(res){
					alert("could not Post new ONE");
				}
		)
	}
	
	$scope.doDelete = function(id){
		$http.delete(url + "/" + id).then(
			function success(res){
				getAll();
			},
			function error(res){
				alert("Couln't delete the record.")
			}
		);
	}
	
	$scope.goToEdit = function(id){
		$location.path("/edit/" + id);
	}
	
	$scope.doSearch = function(){
		$scope.pageNum = 0;
		getAll();
	}
	$scope.doSomething = function(id){
		var promise = $http.post(url + "/" + id);
		promise.then(
			function success(){
				alert("Uspešno uradjeno nesto.")
				getAll();
			},
			function error(){
				alert("Neuspešnu uradjeno nesto.");
				getAll();
			}
		);
	}
	
	
	
	$scope.changePage = function(direction){//-1 unazad, +1 unapred
		$scope.pageNum = $scope.pageNum + direction;
		getAll();
		
	}
	
	
	
});